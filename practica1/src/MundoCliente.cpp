// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>

#include <iostream>
#include <pthread.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	//Cierre de la tuberia. En el cliente no hace falta
	/*char cierre[200];
	sprintf(cierre,"---Cerrando tenis...---");
	write(fd_in,cierre,strlen(cierre)+1);
	close(fd_in);*/
	
	//Cierre proyeccion memoria
	munmap(pMemComp,sizeof(MemComp));
	
	//Cierre bot
	//pMemComp->accion=5;
	
	//Cierre tuberia cliente-servidor comunicacion y fifo
	//close(tuberiacs);
	//unlink("/tmp/CSFifo");
	//Cierre tuberia cliente-servidor teclas y fifo
	//close(tuberiateclas);
	//unlink("/tmp/TeclasFifo");
}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	esfera.Mueve(0.025f);
	int i;
	for(i=0;i<paredes.size();i++)
	{
		paredes[i].Rebota(esfera);
		paredes[i].Rebota(jugador1);
		paredes[i].Rebota(jugador2);
	}

	jugador1.Rebota(esfera);
	jugador2.Rebota(esfera);
	if(fondo_izq.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
		puntos2++;
		
		//Mensaje jugador 2 marca
		/*char cad[200];
		sprintf(cad,"\nJugador 2 marca 1 punto,lleva %d",puntos2);
		write(fd_in,cad,strlen(cad)+1);*/
	}

	if(fondo_dcho.Rebota(esfera))
	{
		esfera.centro.x=0;
		esfera.centro.y=rand()/(float)RAND_MAX;
		esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
		esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
		puntos1++;

		//Mensaje jugador 1 marca
		/*char cad[200];
		sprintf(cad,"\nJugador 1 marca 1 punto,lleva %d",puntos1);
		write(fd_in,cad,strlen(cad)+1);*/
	}
	

	//std::cout << "Hola BOT\n";
	//Bot
	switch(pMemComp->accion)
	{
		case 1:OnKeyboardDown('w',0,0);
		       break;
		case -1:OnKeyboardDown('s',0,0);
		       break;
		case 0:break;
		       
	}
	//Actualizamos los valores de la pelota y las raquetas
	pMemComp->esfera=esfera;
	pMemComp->raqueta1=jugador1;
	pMemComp->raqueta2=jugador2;

	int EstadoActual=0;
	int EstadoAnterior=0;
	if(puntos1==4)
	{
		/*EstadoActual=1;
		if(EstadoActual!=EstadoAnterior)
		{
			char cad[200];
			sprintf(cad,"Jugador 1 marca 4 puntos y gana la partida");
			write(fd_in,cad,strlen(cad)+1);
		}*/
		EstadoAnterior=EstadoActual;
		exit(0);
	}
	
	if(puntos2==4)
	{
		/*EstadoActual=1;
		if(EstadoActual!=EstadoAnterior)
		{
			char cad[200];
			sprintf(cad,"Jugador 2 marca 4 puntos y gana la partida");
			write(fd_in,cad,strlen(cad)+1);
		}*/
		EstadoAnterior=EstadoActual;
		exit(0);
	}
	
	//Comunicacion Cliente-Servidor
	//Leer los datos que nos manda el servidor
	/*char cadcs[200];
	int leer;
	leer=read(tuberiacs,cadcs,sizeof(cadcs));
	//std::cout << "Vamos a leer\n";
	sscanf(cadcs,"%f %f %f %f %f %f %f %f %f %f %d %d",&esfera.centro.x,&esfera.centro.y,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2,&puntos1,&puntos2);
	//std::cout << "He leido: " << leer << "\n";	
	//std::cout << "CS: " << cadcs << "\n";*/


}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	

	
	//if (glutGetModifiers == GLUT_ACTIVE_ALT)
		switch(key)
		{
		//case 'a':jugador1.velocidad.x=-1;break;
		//case 'd':jugador1.velocidad.x=1;break;
		case 's':jugador1.velocidad.y=-4;break;
		case 'w':jugador1.velocidad.y=4;break;
		case 'l':jugador2.velocidad.y=-4;break;
		case 'o':jugador2.velocidad.y=4;break;
		
		}


}

void CMundo::Init()
{
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	/*Abrimos la tuberia del logger
	fd_in=open("/tmp/MiFifo",O_WRONLY);
	if(fd_in==-1){
		perror("open");
		exit;
	}*/
	
	//Para el bot
	int file=open("/tmp/datosBot.txt",O_RDWR|O_CREAT|O_TRUNC,0777);//Creacion del fichero
	write(file,&MemComp,sizeof(MemComp)); //Para escribir en el fichero
        pMemComp=(DatosMemCompartida*)mmap(NULL,sizeof(MemComp),PROT_WRITE|PROT_READ,MAP_SHARED,file,0);//Asignacion del puntero de proyeccion
	close(file);//Cerramos fichero
	//Al principio la accion 0
	pMemComp->accion=0;
	
	//Cliente-servidor comunicacion
	int fifo1;
	fifo1=mkfifo("/tmp/CSFifo",0600);
	if(fifo1==-1)
	{
		perror("mkfifo");
		unlink("/tmp/CSFifo");
		exit(0);
	}
	
	tuberiacs=open("/tmp/CSFifo",O_RDONLY);
	
	if(tuberiacs==-1){
		perror("open");
		close(tuberiacs);
		exit(0);
	}
	
	//Cliente-servidor teclas
	/*int fifo2;
	fifo2=mkfifo("/tmp/TeclasFifo",0600);
	if(fifo2==-1)
	{
		perror("mkfifo");
		unlink("/tmp/TeclasFifo");
		exit(0);
	}
	
	tuberiateclas=open("/tmp/TeclasFifo",O_WRONLY);
	
	if(tuberiateclas==-1){
		perror("open");
		close(tuberiateclas);
		exit(0);
	}*/

	//Practica5
	char nombre_cliente[100];
	printf("Introduzca su nombre de cliente a continuación\n");
	gets(nombre_cliente);

	socket_comunicacion.Connect((char *)"127.0.0.01",4800);
	socket_comunicacion.Send(nombre_cliente, sizeof(nombre_cliente));


}
